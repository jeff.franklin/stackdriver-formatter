# stackdriver-formatter

This formatter will structure a log format message into a JSON format that
can be readily digested by Google Cloud Platform Stackdriver.

## Setting up the formatter

You can set up logging as you normally would with python's `logging`
utility. We also provide a convenience utility for straightforward setup.
One such example:

```python
from stackdriver_formatter import configure_logging

configure_logging(service_name='my_service', version='1.2.3')
```
