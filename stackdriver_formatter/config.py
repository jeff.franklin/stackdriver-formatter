"""Configuration utility for common logging initialization."""
import os
import sys
import logging
from .formatter import StackdriverJsonFormatter


def configure_logging(*args, log_output='json', **kwargs):
    """
    Configure logging, passing all arguments to StackdriverJsonFormatter.
      log_output - set to something other than json to use console logging
    """
    handler = logging.StreamHandler(sys.stdout)
    if os.getenv('LOG_OUTPUT', log_output) == 'json':
        handler.setFormatter(StackdriverJsonFormatter(*args, **kwargs))
    logger = logging.getLogger()
    logger.addHandler(handler)
    log_level = os.getenv('LOG_LEVEL', 'DEBUG')
    logger.setLevel(log_level)
    for handler in logger.handlers:
        handler.setLevel(log_level)
