"""
Stackdriver Formatter
Format log messages for better visibility in Stackdriver
"""
import logging
from pythonjsonlogger import jsonlogger


class StackdriverJsonFormatter(jsonlogger.JsonFormatter):
    """format logs as JSON"""
    def __init__(self, service_name='unknown_service',
                 version='unknown_version', context=None,
                 fmt='%(levelname) %(levelno) %(message)', *args, **kwargs):
        self.context = context or NullContext()
        self.service_name = service_name
        self.version = version
        super().__init__(fmt=fmt, *args, **kwargs)

    def process_log_record(self, log_record: dict):
        log_record['severity'] = log_record.pop('levelname', 'DEBUG')

        # service/version aid Error Reporting but may be useful elsewhere
        log_record['serviceContext'] = {
            'service': self.service_name,
            'version': self.version
        }
        self.format_request_data(log_record)

        if log_record.pop('levelno', 0) >= logging.ERROR:
            self.format_for_error(log_record)

        return super().process_log_record(log_record)

    def format_request_data(self, log_record: dict):
        """Update log_record with request data if we have an active request."""
        request = self.context.request.get(None)
        if not request:
            return
        log_record['requester'] = request.remote_addr
        if 'X-Cloud-Trace-Context' in request.headers:
            log_record['traceContext'] = request.headers['X-Cloud-Trace-Context']
        if 'X-Endpoint-Api-Project-Id' in request.headers:
            log_record['projectNumber'] = request.headers['X-Endpoint-Api-Project-Id']

    def format_for_error(self, log_record: dict):
        """
        Format a `log_record` in a way that it gets recognized by Stackdriver Error Reporting.
        Reference: https://cloud.google.com/error-reporting/docs/formatting-error-messages

        The @type attribute shouldn't be needed if there's a stack_trace attribute,
        but we'll add it in case we log an error with no Traceback.
        'v1beta1' suggests this will be subject to change.
        """
        log_record['@type'] = 'type.googleapis.com/google.devtools.clouderrorreporting.v1beta1.ReportedErrorEvent'

        if 'exc_info' in log_record:
            log_record['stack_trace'] = log_record.pop('exc_info')
        elif 'stack_info' in log_record:
            log_record['stack_trace'] = log_record.pop('stack_info')

        if self.context.request.get(None):
            request = self.context.request.get()
            request_context = {
                'method': request.method,
                'url': request.url,
                'remoteIp': request.remote_addr
            }
            log_record['context'] = {'httpRequest': request_context}


class NullContext:
    """Empty context to default to in the event a context isn't passed in."""
    request = {}
