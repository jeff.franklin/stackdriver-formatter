import os
import re
from setuptools import setup
BASE_DIR = os.path.abspath(os.path.dirname(__file__))
VERSION_FILE = os.path.join(BASE_DIR, 'stackdriver_formatter', 'VERSION')


def strip_comments(line): return line.split('#')[0]


VERSION = ''.join(map(strip_comments, open(VERSION_FILE).readlines())).strip()
VERSION = re.sub('^v', '', VERSION)

desc = 'A common formatter for applications logging stdout to GCP Stackdriver.'
long_description = open(os.path.join(BASE_DIR, 'README.md')).read()


setup(name='stackdriver-formatter',
      install_requires=['python-json-logger'],
      setup_requires=['pytest-runner'],
      tests_require=['pytest', 'pytest-cov', 'pytest-flake8'],
      url='https://gitlab.com/jeff.franklin/stackdriver-formatter',
      author='Woolpert Cloud Solutions',
      author_email='jeff.franklin@woolpert.com',
      version=VERSION,
      description=desc,
      license='Apache License, Version 2.0',
      long_description=long_description,
      long_description_content_type="text/markdown",
      packages=['stackdriver_formatter'],
      package_data={'stackdriver_formatter': ['VERSION']},
      python_requires='>=3.6',
      classifiers=[
        'License :: OSI Approved :: Apache Software License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3.6',
        'Framework :: Pytest']
      )
