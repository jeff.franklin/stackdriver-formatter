"""Stackdriver Formatter tests."""
import unittest.mock
from stackdriver_formatter import StackdriverJsonFormatter


def test_stackdriver_json_formatter_error(monkeypatch):
    """Check that we correctly format errors."""
    context_var = unittest.mock.MagicMock()
    request = context_var.request.get.return_value
    request.method = 'DELETE'
    request.url = 'http://localhost/foo'
    request.remote_addr = '192.168.1.1'
    request.headers = {
        'X-Cloud-Trace-Context': 'my_trace_context',
        'X-Endpoint-Api-Project-Id': '54321'
    }

    formatter = StackdriverJsonFormatter('test_service', '3.2.1', context_var)
    log_record = {
        'levelname': 'ERROR',
        'levelno': 40,
        'message': 'we died',
        'exc_info': 'Traceback',
        'extra': 'attribute'
    }

    response = formatter.process_log_record(log_record)

    assert response == {
        'severity': 'ERROR',
        'message': 'we died',
        '@type': 'type.googleapis.com/google.devtools.clouderrorreporting.v1beta1.ReportedErrorEvent',
        'serviceContext': {
            'service': 'test_service',
            'version': '3.2.1'
        },
        'requester': '192.168.1.1',
        'traceContext': 'my_trace_context',
        'projectNumber': '54321',
        'stack_trace': 'Traceback',
        'context': {
            'httpRequest': {
                'method': 'DELETE',
                'url': 'http://localhost/foo',
                'remoteIp': '192.168.1.1'
            }
        },
        'extra': 'attribute'
    }
